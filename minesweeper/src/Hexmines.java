import java.awt.*;
import javax.swing.*;



public class Hexmines
{
	//Defining variables below

	public static boolean CordXY=true;	//making the x and y coordinates of the rectangle to true, if they are false make x and y the coordinates of the top left rectangle
	private static int Pixel_Border=50;	//defining border for pixels
	private static int len_oneside=0;	// One side length
	private static int side_triangle=0;	// side length of triangle for hex
	private static int radius_circ=0;	// radius of center to middle circle
	private static int height_hex=0;	// Distance of heights between hex

	public static void setCordXY(boolean b) {
		CordXY=b;
	}
	public static void setPixelBorder(int b){
		Pixel_Border=b;
	}

	
	public static void setLenofSide(int side) {  //this function calculates dimensions of the hex sides
		len_oneside=side;
		side_triangle =  (int) (len_oneside / 2);			
		radius_circ =  (int) (len_oneside * 0.8660254037844);	
		height_hex=2*radius_circ;
	}
	public static void setDistanceHeight(int height) { //this function calculates distance of the hex sides
		height_hex = height;			
		radius_circ = height_hex/2;			
		len_oneside = (int) (height_hex / 1.73205);	
		side_triangle = (int) (radius_circ / 1.73205);	
	}


	public static Polygon hex (int x1, int y1) { //this function calculates all six sides in a hexagon

		int y = y1 + Pixel_Border;
		int x = x1 + Pixel_Border; 
				      
		if (len_oneside == 0  || height_hex == 0) {
			System.out.println("Size of hex is not right");
			return new Polygon();
		}

		int[] circ_x,circ_y;

		if (CordXY) //this section below configures the placement of hexagons 
			circ_x = new int[] {x,x+len_oneside,x+len_oneside+side_triangle,x+len_oneside,x,x-side_triangle};  
		else
			circ_x = new int[] {x+side_triangle,x+len_oneside+side_triangle,x+len_oneside+side_triangle+side_triangle,x+len_oneside+side_triangle,x+side_triangle,x};

		circ_y = new int[] {y,y,y+radius_circ,y+radius_circ+radius_circ,y+radius_circ+radius_circ,y+radius_circ};
		return new Polygon(circ_x,circ_y,6);
	}



	public static void drawHex(int len, int heigh, Graphics2D g2) { //this function handles the graphics by drawing hexagon
		int x = len * (len_oneside+side_triangle);
		int y = heigh * height_hex + (len%2) * height_hex/2;
		Polygon poly = hex(x,y);
		g2.setColor(Color.LIGHT_GRAY);
		//g2.fillPolygon(hexmech.hex(x,y));
		g2.fillPolygon(poly);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(poly);
	}

	public static void fillHex(int len, int heigh, int a, Graphics2D g2) {  //this function fills the hexagons with color and strings
		char ch='l';
		int x = len * (len_oneside+side_triangle);
		int y = heigh * height_hex + (len%2) * height_hex/2;
		if (a < 0) {
			g2.setColor(new Color(255,255,255,200));
			g2.fillPolygon(hex(x,y));
			g2.setColor(Color.blue);
			ch = (char)(-a);
			g2.drawString(""+ch, x+radius_circ+Pixel_Border, y+radius_circ+Pixel_Border+4); 
			
		}
		if (a > 0) {
			g2.setColor(Color.WHITE);
			g2.fillPolygon(hex(x,y));
			g2.setColor(Color.BLACK);
			ch = (char)a;
			g2.drawString(""+ch, x+radius_circ+Pixel_Border, y+radius_circ+Pixel_Border+4); 
		
		}
	}

	
	public static Point PixeltoHex(int mousex, int mousey) { //this function migrates the position of pixels from a mouse event to another position of grid
		Point pointer = new Point(-1,-1);

		
		mousex -= Pixel_Border;
		mousey -= Pixel_Border;
		if (CordXY) mousex += side_triangle;

		int x = (int) (mousex / (len_oneside+side_triangle)); 
		int y = (int) ((mousey - (x%2)*radius_circ)/height_hex); 

		int pixelx = mousex - x*(len_oneside+side_triangle); //this fixes the click of hexagons on the left side
		int pixely = mousey - y*height_hex; // this fixes the click of hexagons on the right side

		if (mousey - (x%2)*radius_circ < 0) return pointer; // prevents the user to not click on top of hex

		
		//this section below layouts the coloumns perfectly
		if (x%2==0) {
			if (pixely > radius_circ) {	
				if (pixelx * radius_circ /side_triangle < pixely - radius_circ) {
					x--;
				}
			}
			if (pixely < radius_circ) {	
				if ((side_triangle - pixelx)*radius_circ/side_triangle > pixely ) {
					x--;
					y--;
				}
			}
		} else {  
			if (pixely > height_hex) {
				if (pixelx * radius_circ/side_triangle < pixely - height_hex) {
					x--;
					y++;
				}
			}
			if (pixely < height_hex) {	
				
				if ((side_triangle - pixelx)*radius_circ/side_triangle > pixely - radius_circ) {
					x--;
				}
			}
		}
		pointer.x=x;
		pointer.y=y;
		return pointer;
	}
}