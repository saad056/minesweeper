
public class Main implements Runnable {
	
	Grid gui = new Grid();  //This calls the Grid class and creates gui object
	public static void main(String[] args) {
		new Thread(new Main()).start();
	}

	@Override
	public void run() {
		while(true) {
			gui.repaint(); // We are running the gui continously
			if(gui.resetter == false) { //if the gui has not been reset
			gui.checkVictoryStatus(); //check the status of the vicotory to check if the player has won or lost the game
			
			}
		}
		
	}

}
