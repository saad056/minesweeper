import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


public class Grid extends JFrame{
//This class mainly focuses on designing the GUI of the game
	//variables defined below
	public boolean resetter = false; 
	public boolean flagger = false;
	Date startDate = new Date();
	Date endDate;
	
	int spacing = 1 ;
	int neighs = 0;
	
	String vicMes = "Nothing yet!";
	
	public int mx = -100;
	public int my = -100;
	
	public int smileyX = 605;
	public int smileyY = 5;
	
	public int smileyCenterX = smileyX + 35;
	public int smileyCenterY = smileyY + 35;
	
	public int flaggerX = 445;
	public int flaggerY = 5;
	
	public int flaggerCenterX = flaggerX + 35;
	public int flaggerCenterY = flaggerY + 35;
	
	public int timeX = 1130;
	public int timeY = 5;
	
	public int vicMesX = 740;
	public int vicMesY = -50;
	
	public int sec = 0;
	
	
	public boolean happiness = true;
	
	public boolean victory = false;
	
	public boolean defeat = false;
	
	Random rand = new Random(); //generating a random number
	
	int[][] mines = new int[16][9]; //array for generating mines
	int [][]neighbours = new int[16][9]; //array for generating near by mines
	boolean[][] revealed = new boolean[16][9]; //array for revealing mines
	boolean[][] flagged = new boolean[16][9]; //array for flagging squares
	
	
	public Grid() {
		this.setTitle("Minesweeper");   //this sets the title of the window frame
		this.setSize(1286,829); //this sets the size of the frame
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //this indicates to close open exiting
		this.setVisible(true); //setting the window frame to be visible
		this.setResizable(false); //the window frame will not be resizable
		
		for(int i = 0; i< 16; i++) {  //these for loops generate the dimension and number of squares to be created
			
			for (int j = 0; j< 9; j++) {
				if(rand.nextInt(100)<20) { //if the random number is less than 20 than append 1 to mines array (generate mines on the squares)
					mines[i][j] = 1;       
				}  else {     //if the random number is not less than 20 than append 0 to mines array
					mines[i][j] = 0;
				}
				revealed[i][j] = false; //setting this to false as when the mouse if clicked, this becomes true and the squares are exposed
			}
			
		}
		
		for(int i = 0; i< 16; i++) {  //these for loops check if there are any neighbours if so then they are generated
			for (int j = 0; j< 9; j++) { 
				neighs = 0;
				for(int m=0; m<16; m++) {
					
					for(int n = 0; n<9; n++) {
						if(!(m == i && n == j)) {
						if (isN(i,j,m,n) == true)
							neighs++;
					}
				}
				}
				neighbours[i][j] = neighs;
			}
			
		}
		Board board = new Board();  //creating an object by calling Board class
		this.setContentPane(board);
		
		
		Select click = new Select(); //calls the select class
		this.addMouseListener(click); //adds a mouse listener to the click object
	}
	
	public class Board extends JPanel {
		public void paintComponent(Graphics g) {
			g.setColor(Color.DARK_GRAY); //This is coloring the Grid created below
			g.fillRect(0, 0, 1280, 800); //this is creating a background Grid to create the squares inside
			
			for(int i = 0; i< 16; i++) {  //these for loops generate the dimension and number of squares to be created
				
				for (int j = 0; j< 9; j++) { 
					g.setColor(Color.GRAY); //this is coloring the squares created
					 if (revealed[i][j] == true) { //if the revealed array has true in it than generate a block by colouring it white
						g.setColor(Color.WHITE);
						if(mines[i][j] == 1) { //if there is a mine than color the mine red
							g.setColor(Color.red); 
						}
					}
					if(mx >= spacing+i*80 && mx < spacing+i*80+80-2*spacing &&  my >= spacing+j*80+80+26 && my <spacing+j*80+26+80+80-2*spacing)//this highlights the squares and assigns red colour to them so we know that the mouse is where the square is 
					{
						g.setColor(Color.LIGHT_GRAY);       //if the squares are created with correct spacing than colour them with Light gray
					}
					g.fillRect(spacing+i*80, spacing+j*80+80, 80-2*spacing, 80-2*spacing); //this method creates squares with equal spacing between them
					if (revealed[i][j] == true) { //if the revealed array has true in it than generate a number by colouring it black
						g.setColor(Color.BLACK);
						if(mines[i][j]==0 && neighbours[i][j] != 0) { //if there are no mines and there are neighburs near by
						if(neighbours[i][j] == 1) { //if the neighbour is equal to 1
							g.setColor(Color.blue); //Color it blue
						} else if(neighbours[i][j] == 2) { //if the neighbour is equal to 2
							g.setColor(Color.green); //Color it green
						} else if(neighbours[i][j] == 3) { //if the neighbour is equal to 3
							g.setColor(Color.red); //Color it red
						} else if(neighbours[i][j] == 4) { //if the neighbour is equal to 4
							g.setColor(new Color(0,0,128)); //Color it maroon
						} else if(neighbours[i][j] == 5) { //if the neighbour is equal to 5
							g.setColor(new Color(178,34,34)); //Color it 
						} else if(neighbours[i][j] == 6) { //if the neighbour is equal to 6
							g.setColor(new Color(72,209,204)); //Color it
						} else if(neighbours[i][j] == 8) { //if the neighbour is equal to 8
							g.setColor(Color.darkGray); //Color it darkGray
						}
						g.setFont(new Font("Tahoma", Font.BOLD, 40)); //set the font of text displayed
						g.drawString(Integer.toString(neighbours[i][j]), i*80+27, j*80+80+55); //displaying numbers on squares
					} else if(mines[i][j] == 1){   //else if its a mine, draw a mine
						 g.fillRect(i*80+10+20, j*80+80+20, 20, 40);
						 g.fillRect(i*80+20, j*80+80+10+20, 40, 20);
						 g.fillRect(i*80+5+20, j*80+80+5+20, 30, 30);
						 g.fillRect(i*80+38, j*80+80+15, 4, 50);
						 g.fillRect(i*80+15, j*80+80+38, 50, 4);
						}
						
					}
					
					//Creating flags n the squares when the flag button is selected
					if(flagged[i][j] == true) { //if the flag button is selected, draw it
						g.setColor(Color.black);
						g.fillRect(i*80+32+5, j*80+80+15+5, 7, 40);
						g.fillRect(i*80+20+5, j*80+80+50+5, 30, 10 );
						g.setColor(Color.red);
						g.fillRect(i*80+16+5, j*80+80+15+5, 20, 15);
						g.setColor(Color.black);
						g.drawRect(i*80+16+5, j*80+80+15+5, 20, 15);
						g.drawRect(i*80+17+5, j*80+80+16+5, 18, 13);
						g.drawRect(i*80+18+5, j*80+80+17+5, 16, 11);
					}
				}
			
			}
			
			
			//Creating a smiley face button below:
			g.setColor(Color.yellow);
			g.fillOval(smileyX, smileyY, 70, 70);
			g.setColor(Color.BLACK);
			g.fillOval(smileyX+15, smileyY+20, 10, 10);
			g.fillOval(smileyX+45, smileyY+20, 10, 10);
			if(happiness == true) { //if the game is still going create a smiley face 
				g.fillRect(smileyX+20, smileyY+50, 30, 5);
				g.fillRect(smileyX+17, smileyY+45, 5, 5);
				g.fillRect(smileyX+48, smileyY+45, 5, 5);
			} else { //if the person has lost the game, create a sad face
				g.fillRect(smileyX+20, smileyY+45, 30, 5);
				g.fillRect(smileyX+17, smileyY+50, 5, 5);
				g.fillRect(smileyX+48, smileyY+50, 5, 5);
			}
			
			//Creating a flag button on top
			g.setColor(Color.black);
			g.fillRect(flaggerX+32, flaggerY+15, 7, 40);
			g.fillRect(flaggerX+20, flaggerY+50, 30, 10 );
			g.setColor(Color.red);
			g.fillRect(flaggerX+16, flaggerY+15, 20, 15);
			g.setColor(Color.black);
			g.drawRect(flaggerX+16, flaggerY+15, 20, 15);
			g.drawRect(flaggerX+17, flaggerY+16, 18, 13);
			g.drawRect(flaggerX+18, flaggerY+17, 16, 11);
			
			 //Creating a box to display time and the seconds below
			if(flagger == true) {
				g.setColor(Color.red);
			}
			
			g.drawOval(flaggerX,flaggerY, 70, 70);
			g.drawOval(flaggerX+1,flaggerY+1, 68, 68);
			g.drawOval(flaggerX+2,flaggerY+2, 66, 66);
			
			g.setColor(Color.black);
			g.fillRect(timeX, timeY, 140, 70);
			if (defeat == false && victory == false) { //if the person has not lost the game, start the timer
			sec = (int)((new Date().getTime()-startDate.getTime()) / 1000);
			}
			if (sec > 999) {
				sec = 999;
			}
			g.setColor(Color.WHITE);  //Coloring and creating the time below:
			g.setFont(new Font("Tahoma",Font.PLAIN,80));
			if (sec < 10) {
				g.drawString("00"+Integer.toString(sec),timeX,timeY+65);
			} else if (sec < 100) {
			    g.drawString("0"+Integer.toString(sec),timeX,timeY+65);
			} else {
				g.drawString(Integer.toString(sec),timeX,timeY+65);
			}
			  //If the player wins, create a victory message:
			if (victory == true) {
				g.setColor(Color.GREEN);
				vicMes = "YOU WIN";
			} else if (defeat == true) {
				g.setColor(Color.red);
				vicMes = "YOU LOSE";
			}
			
			if(victory == true || defeat == true) {
				vicMesY = -50 + (int) (new Date().getTime() - endDate.getTime()) / 10;
				if (vicMesY > 67) {
					vicMesY = 67;
				}
				g.setFont(new Font("Tahoma",Font.PLAIN, 70));
				g.drawString(vicMes, vicMesX, vicMesY);
			}
			
			}
	}
	

	public class Select implements MouseListener {
//this class allows the MouseListener to interact with the squares drawn above and handles events
		@Override
		public void mouseClicked(MouseEvent e) { //this handles the event for when a mouse is clicked
			
			mx = e.getX();
			my = e.getY();
			
			
			if(inBoxX() != -1 && inBoxY() != -1) { //if the mouse clicked is on a box (both in x axis and y axis) then:
			System.out.println("The mouse is in the [" + inBoxX() + "," + inBoxY() + "], Number of mine neighs: " + neighbours[inBoxX()][inBoxY()]); //display the coordinates of that box and number of neighbours around that box
			if (flagger == true && revealed[inBoxX()][inBoxY()] == false) { //if a flag is placed on a box than dont let the box reveal anything:
				if(flagged[inBoxX()][inBoxY()] == false) {
				flagged[inBoxX()][inBoxY()] = true;
				} else {
					flagged[inBoxX()][inBoxY()] = false;
				}
			}  else {
				if(flagged[inBoxX()][inBoxY()] == false) {
					revealed[inBoxX()][inBoxY()] = true; //this checks whether we are inside of a box, if so then reveal the mines
				}
				
			}
			} else {
				System.out.println("The pointer is not inside of any box!"); //this handles the event for when a mouse is clicked
			}
			
			if(inSmiley() == true) { //if the smileybutton is clicked
				resetAllButton(); //reset the game
			}
			
			if(inFlagger() == true) { //message displayed when placing a flag on a box
				if(flagger == false) {
				flagger = true;
				System.out.println("In flagger = true!");
			} else {
				flagger = false;
				System.out.println("In flagger = false!");
			}
			}		
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	//Reset Button function
	
	public void checkVictoryStatus() { //this function operates the program according to what the victory status is
		
		if(defeat == false) {  //if the player has not yet lost the game
		for(int i = 0; i < 16; i++) {
			for(int j = 0; j < 9; j++) {
				if (revealed[i][j] == true && mines[i][j] == 1) {  //if a mine is displayed than indicate that its game over and reset the timer
					defeat = true;
					happiness = false;
					endDate = new Date();
				}
			}
		}
		}
		
		if(totalBoxesRevealed() >= 144 - totalMines() && victory == false) { //if all the boxes have been revealed and no mines are found then make the victory to true
			victory = true;
			endDate = new Date();
		}
	}
	
	public int totalMines() {   //generating total mines
	    int total = 0;
		for(int i = 0; i < 16; i++) {
			for(int j = 0; j < 9; j++) {
				if (mines[i][j] == 1) {
					total++;
				}
			}
		}
		return total;
	}
	
	public int totalBoxesRevealed() { //generating total revealed boxes
		int total = 0;
		for(int i = 0; i < 16; i++) {
			for(int j = 0; j < 9; j++) {
				if (revealed[i][j] == true) {
					total++;
				}
			}
		}
		return 0;
	}
	
	public void resetAllButton() { //this is the smiley button which resets every component
		
		resetter = true;
		startDate = new Date();
		flagger = false;
		vicMesY = -50;
		vicMes = "Nothing yet!";
		happiness = true;
		victory = false;
		defeat = false;
		
for(int i = 0; i< 16; i++) {  //these for loops generate the dimension and number of squares to be created
			
			for (int j = 0; j< 9; j++) {
				if(rand.nextInt(100)<20) { //if the random number is less than 20 than generate mines on the squares
					mines[i][j] = 1;
				}  else {
					mines[i][j] = 0;
				}
				revealed[i][j] = false;
				flagged[i][j] = false;
			}
			
		}
		
		for(int i = 0; i< 16; i++) {  //these for loops check if there are any neighbours if so then they are generated
			for (int j = 0; j< 9; j++) { 
				neighs = 0;
				for(int m=0; m<16; m++) {
					
					for(int n = 0; n<9; n++) {
						if(!(m == i && n == j)) {
						if (isN(i,j,m,n) == true)
							neighs++;
					}
				}
			}
				neighbours[i][j] = neighs;
			}
			
		}
			resetter = false;
	}
	//reset button actual
	public boolean inSmiley() { //this is the smiley button whichis linked to the reset button
		int dif = (int) Math.sqrt(Math.abs(mx-smileyCenterX)*Math.abs(mx-smileyCenterX)+Math.abs(my-smileyCenterY)*Math.abs(my-smileyCenterY));
		if (dif < 35) {
			return true;
		}
		return false;
	}
	
	public boolean inFlagger() { //if the flagger button is selected
		int dif = (int) Math.sqrt(Math.abs(mx-flaggerCenterX)*Math.abs(mx-flaggerCenterX)+Math.abs(my-flaggerCenterY)*Math.abs(my-flaggerCenterY));
		if (dif < 35) {
			return true;
		}
		return false;
	}
	
	public int inBoxX() { //this function checks that if the mouse is in the x-axis of the box
		for(int i = 0; i< 16; i++) {  
			
			for (int j = 0; j< 9; j++) { 
				if(mx >= spacing+i*80 && mx < spacing+i*80+80-2*spacing &&  my >= spacing+j*80+80+26 && my <spacing+j*80+26+80+80-2*spacing)//this highlights the squares and assigns red colour to them so we know that the mouse is where the square is 
				{
				return i;
				}
			
			}
		}
		return -1;
	}
	public int inBoxY() { //this functions checks that if the mouse is in the y-axis of the box
for(int i = 0; i< 16; i++) {  
			
			for (int j = 0; j< 9; j++) { 
				if(mx >= spacing+i*80 && mx < spacing+i*80+80-2*spacing &&  my >= spacing+j*80+80+26 && my <spacing+j*80+26+80+80-2*spacing)//this highlights the squares and assigns red colour to them so we know that the mouse is where the square is 
				{
				return j;
				}
			
			}
		}
		return -1;
	}
	public boolean isN(int mX, int mY, int cX, int cY) { //this function checks if the squares have any neighbours or not
		if(mX - cX < 2 && mX - cX > - 2 && mY - cY <2 && mY - cY > -2 && mines[cX][cY] == 1) {
			return true;
		}
		return false;
	}
	
}
