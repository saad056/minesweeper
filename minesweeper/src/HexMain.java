import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Random; 


public class HexMain
{
  private HexMain() {
		initGame();
		GUI();
	}

	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
				new HexMain(); //allows to run this class
				}
				});
	}


	final static int Board_size = 12; //final board size.
	final static int HexSizePixels = 60;	//final hexagon size in pixels
	final static int SideBorders = 15;  
	final static int ScreenSize = HexSizePixels * (Board_size + 1) + SideBorders*3; //Size of screen vertically

	int[][] Grid = new int[Board_size][Board_size];

	void initGame(){ //this function starts the game

		Hexmines.setCordXY(false); 

		Hexmines.setDistanceHeight(HexSizePixels); //setting the height
		Hexmines.setPixelBorder(SideBorders); //setting the borders

		for (int i=0;i<Board_size;i++) {  //generating the board
			for (int j=0;j<Board_size;j++) {
				Grid[i][j]=0;
			}
		}
	
		//set up board here
//		board[3][3] = (int)'A';
//		board[4][3] = (int)'Q';
//		board[4][4] = -(int)'B';
	}

	private void GUI() //this class draws the GUI of the game
	{
		Panel panel = new Panel(); //calls and draws a panel
		JFrame frame = new JFrame("HexMines");
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		Container content = frame.getContentPane();
		content.add(panel);
		frame.setSize( (int)(ScreenSize/1.23), ScreenSize);
		frame.setResizable(false);
		frame.setLocationRelativeTo( null );
		frame.setVisible(true);
	}


	class Panel extends JPanel
	{		
		

		public Panel() //drawing the panel and adding mouseevents 
		{	
			setBackground(Color.GRAY);

			MouseList Me = new MouseList();            
			addMouseListener(Me);
		}

		public void paintComponent(Graphics g) //painting the string to be displayed when a box is clicked
		{
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setFont(new Font("Tahoma", Font.PLAIN, 17));
			super.paintComponent(g2);
			//draw grid
			for (int i=0;i<Board_size;i++) {
				for (int j=0;j<Board_size;j++) {
					Hexmines.drawHex(i,j,g2);
				}
			}
			//fill the Hexes
			for (int i=0;i<Board_size;i++) {
				for (int j=0;j<Board_size;j++) {					
					
					Hexmines.fillHex(i,j,Grid[i][j],g2);
				}
			}


		}
		
		class MouseList extends MouseAdapter	{	//Handles the mouse events
			public void mouseClicked(MouseEvent e) { 
				int x = e.getX(); 
				int y = e.getY(); 
				
				Point pointer = new Point( Hexmines.PixeltoHex(e.getX(),e.getY()) );
				if (pointer.x < 0 || pointer.y < 0 || pointer.x >= Board_size || pointer.y >= Board_size) return;

				
				//Display the following when a hexagon is clicked
				Grid[pointer.x][pointer.y] = (int)'1';
				repaint();
			}		 
		} 
	} 
}